Node.js Content Sync
===================

Adds full synchronization of visible nodes with content on the server. Whenever
a node is updated by a content editor, all active visitors viewing the node will
see the content replaced with an updated version.

Useful for:
Breaking news events, live webcasts, collaborative editing.

- Supports nested nodes either through node reference fields or node embed input filter.
- Supports nested webform nodes and other dynamic node content.

Usage:
- Enable the module and nodejs integration dependency. All nodes now sync.
