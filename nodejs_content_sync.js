(function ($) {

Drupal.Nodejs.callbacks.nodejsContentSync = {
  callback: function (message) {

    // Fetch the node from the path in the message
    $("[data-nodejs-content-sync-channel='" + message.channel + "']")
    .trigger("nodejs-content-sync:reload");
  }
};

Drupal.behaviors.nodejs_content_sync = {
  attach: function (context, settings) {

    // Keeps a collection of contentSyncAjax objects outside of Drupal's main list
    // in order not to pollute that list with large numbers of objects
    Drupal.Nodejs.contentSyncAjax = Drupal.Nodejs.contentSyncAjax || {};

    $("[data-nodejs-content-sync-channel]", context).each(function(index) {

      // Creates an Ajax object for each syncable content on the page
      var channel = $(this).attr('data-nodejs-content-sync-channel');

      Drupal.Nodejs.contentSyncAjax[channel] = new Drupal.ajax(channel, this, {
        event: "nodejs-content-sync:reload",
        url: "nodejs/contentsync/node" + $(this).attr('data-nodejs-content-sync-uri'),
        progress: {type: 'none'} // Content sync should appear as seamless as possible
      });
    });
  },
  detach: function (context, settings) {

    // Cleans up now useless AJAX objects when removing nodes from view
    $("[data-nodejs-content-sync-channel]", context).each(function(index) {
      var channel = $(this).attr('data-nodejs-content-sync-channel');
      delete Drupal.Nodejs.contentSyncAjax[channel];
    });
  }
}

}(jQuery));
